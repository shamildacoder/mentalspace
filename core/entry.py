import sqlite3
from datetime import datetime

db = sqlite3.connect('.data/entries.db')


def init():
    db.execute("""CREATE TABLE ENTRIES(
            ID INTEGER PRIMARY KEY,
            TIMESTAMP TEXT,
            TITLE VARCHAR(100),
            CONTENT BLOB);""")


class Entry:
    def __init__(self, ID, timestamp, title, content):
        self.ID = ID
        self.title = title
        self.content = content
        self.timestamp = timestamp

    def __repr__(self):
        return "Entry object at " + self.timestamp


def create_entry(title, content):
    # Get the current time as a timestamp and remove microseconds.
    timestamp = str(datetime.today().replace(microsecond=0))
    db.execute(f'INSERT INTO ENTRIES(TIMESTAMP, TITLE, CONTENT) VALUES("{timestamp}", "{title}", "{content}")')
    db.commit()


def get_entry_by_ID(ID):
    try:
        entry_raw = db.execute('SELECT * FROM ENTRIES WHERE ID=' + str(ID).fetchone())
    except TypeError:  # When no entry matching ID is found, None is returned raising a TypeError.
        return 'Not found.'
    ID, timestamp, title, content = entry_raw
    entry = Entry(ID, timestamp, title, content)
    return entry


def get_entries():
    entries = list(db.execute('SELECT * FROM ENTRIES').fetchall())
    entry_list = []
    for entry in entries:
        # Create an Entry object with TIMESTAMP, TITLE and CONTENT from the entry list
        ID, timestamp, title, content = entry
        entry = Entry(ID, timestamp, title, content)
        entry_list.append(entry)

    return entry_list


def edit_entry(ID, title=None, content=None):
    db.execute(f'UPDATE ENTRIES SET TITLE="{title}", CONTENT="{content}" WHERE ID=' + str(ID))
    db.commit()


def delete_entry(ID):
    db.execute('DELETE FROM ENTRIES WHERE ID=' + str(ID))
    db.commit()
