from Crypto.Cipher import AES
from hashlib import md5


def _genkey_and_IV(key):
    # I'd use SHA256 instead but it doesn't return a 32 byte hash
    # but MD5 does.
    key = md5(key.encode()).hexdigest()
    # I don't know how I'm supposed to save the IV in something other than
    # a text file or in the SQLite database. So, I use a blank IV.
    IV = '0' * 16
    return key, IV


def encrypt(data, key):
    key, IV = _genkey_and_IV(key)
    encryptor = AES.new(key, AES.MODE_CFB, IV=IV)
    return encryptor.encrypt(data)


def decrypt(data, key):
    key, IV = _genkey_and_IV(key)
    decryptor = AES.new(key, AES.MODE_CFB, IV=IV)
    return decryptor.decrypt(data)


